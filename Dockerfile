FROM node:16

WORKDIR /restbase

RUN set -eux; \
    git clone \
        --branch master \
        --single-branch \
        https://github.com/wikimedia/restbase.git . ; \
    git reset --hard 2f010d0a5b80ddbf21d7d0f4b581bc70d2d62128 ; \
    npm install ; \
    useradd -U -r -s /bin/bash restbase ; \
    mkdir -p /data ; \
    chown -R restbase:restbase /data ; \
    mkdir -p /home/restbase || true ; \
    chown -R restbase:restbase /home/restbase

VOLUME /data

COPY entrypoint.sh /restbase/entrypoint.sh
RUN chmod +x /restbase/entrypoint.sh

ENTRYPOINT ["/restbase/entrypoint.sh"]
